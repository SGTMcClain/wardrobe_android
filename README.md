## Future enhancements
1. Utilize on device storage/database (ROOM) to store data before sending it to Firebase. Follow this blog post [Seven Steps to Room](https://medium.com/google-developers/7-steps-to-room-27a5fe5f99b2)

## References
1. [Firebase Realtime Database CRUD Operation for Android](https://www.simplifiedcoding.net/firebase-realtime-database-crud/) by Simplified Coding
2. Youtube Playlist [Firebase Realtime Database Tutorial for Android](https://www.youtube.com/playlist?list=PLk7v1Z2rk4hj6SDHf_YybDeVhUT9MXaj1) by Simplified Coding
