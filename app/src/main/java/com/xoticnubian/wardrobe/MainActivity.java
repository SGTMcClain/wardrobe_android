package com.xoticnubian.wardrobe;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;


import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "com.xoticnubian.wardrobe.MESSAGE";
    private static final String TAG = "DocSnippets";
    public static final int CAMERA_REQUEST_CODE = 228;                  //Used to pass messages
    public static final int CAMERA_PERMISSION_REQUEST_CODE = 4192;      //Used to get permission to the camera


    EditText editTextItemName;
    EditText editTextItemSubType;
    EditText editTextItemColor;
    EditText editTextItemSize;
    Spinner spinnerItemType;
    Button buttonSave;
    Button buttonAddPhoto;

    DatabaseReference databaseItems;

    ImageView itemImage;

    ListView listViewItems;
    List<Item> itemList;

    //Invoke Camera Intent
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
        }
    }

    // Gets the result of the dispatch Take Picture Intent
    // TODO: Add the ability to save to the file system and gallery on the current device
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            itemImage.setImageBitmap(imageBitmap);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);

        databaseItems = FirebaseDatabase.getInstance().getReference("item");

        editTextItemName = findViewById(R.id.editTextName);
        spinnerItemType = findViewById(R.id.spinnerItemType);
        editTextItemSubType = findViewById(R.id.editSubtype);
        editTextItemColor = findViewById(R.id.editItemColor);
        editTextItemSize = findViewById(R.id.editItemSize);
        buttonSave = findViewById(R.id.editButtonSave);
        buttonAddPhoto = findViewById(R.id.editButtondAddPhoto);

        itemImage = findViewById(R.id.editItemImageView);

        listViewItems = findViewById(R.id.listViewItems);
        itemList = new ArrayList<>();

        buttonSave.setOnClickListener(view -> MainActivity.this.addItem());
        buttonAddPhoto.setOnClickListener(view -> dispatchTakePictureIntent());

//        buttonAddPhoto.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dispatchTakePictureIntent();
//            }
//        });


    }

    @Override
    protected void onStart() {
        super.onStart();

        databaseItems.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                itemList.clear();

                for(DataSnapshot itemSnapshot : dataSnapshot.getChildren()){
                    Item item = itemSnapshot.getValue(Item.class);
                    itemList.add(item);
                }

                ItemList adapter = new ItemList(MainActivity.this, itemList);
                listViewItems.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void addItem(){

        String itemName = editTextItemName.getText().toString().trim();
        String itemType = spinnerItemType.getSelectedItem().toString();
        String itemDescription = editTextItemSubType.getText().toString().trim();
        String itemColor = editTextItemColor.getText().toString().trim();
        String itemSize = editTextItemSize.getText().toString().trim();

        if(!TextUtils.isEmpty(itemName)){
            String id = null;

            try {
                //get an id value
                id = databaseItems.push().getKey();

                //create the item based on what was put in the GUI
                Item item = new Item(id, itemName, itemDescription, itemColor, itemType, itemSize);

                //store the item into a database by the id value
                databaseItems.child(id).setValue(item);

            } catch (NullPointerException e){
                e.printStackTrace();
            }


            Toast.makeText(this, itemName + " added!", Toast.LENGTH_LONG).show();

        } else {
            // make sure that the user has to enter before saving to the database possibly by returning null
            Toast.makeText(this, "Please name your item", Toast.LENGTH_LONG).show();
        }


    }

}
