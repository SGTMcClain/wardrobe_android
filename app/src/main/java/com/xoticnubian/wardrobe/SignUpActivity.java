package com.xoticnubian.wardrobe;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = "SGTMcClain" ;
    EditText editTextEmail;
    EditText editTextUserName;
    EditText editTextPassword;
    EditText editTextLastName;
    EditText editTextFirstName;
    EditText editTextDateOfBirth;

    ProgressBar progressBar;

    // Firebase Authentication
    private FirebaseAuth firebaseAuth;

    // Firebase Database
    DatabaseReference databaseReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);

        setContentView(R.layout.activity_sign_up);

        editTextEmail = findViewById(R.id.editTextEmail);
        editTextUserName = findViewById(R.id.editTextUserName);
        editTextPassword = findViewById(R.id.editTextPassword);
        editTextLastName = findViewById(R.id.editTextLastName);
        editTextFirstName = findViewById(R.id.editTextFirstName);
        editTextDateOfBirth = findViewById(R.id.editTextDateOfBirth);

        progressBar = findViewById(R.id.signUp_progressbar);

        // Firebase Authentication
        firebaseAuth = FirebaseAuth.getInstance();



        findViewById(R.id.buttonSignUp).setOnClickListener(this);
    }

    private void registerUser(){
        String email = editTextEmail.getText().toString().trim();
        String username = editTextUserName.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();
        String firstname = editTextLastName.getText().toString().trim();
        String lastname = editTextFirstName.getText().toString().trim();
        String dateOfBirth = editTextDateOfBirth.getText().toString().trim();

        if (email.isEmpty()) {
            editTextEmail.setError("Email is required to continue");
            editTextEmail.requestFocus();
            return;
        }

        if (password.isEmpty()) {
            editTextPassword.setError("Password is required to continue");
            editTextPassword.requestFocus();
            return;
        }

        if (username.isEmpty()) {
            editTextUserName.setError("Username is required to continue");
            editTextUserName.requestFocus();
            return;
        }

        if (dateOfBirth.isEmpty()) {
            editTextDateOfBirth.setError("Birth date is required to continue");
            editTextDateOfBirth.requestFocus();
            return;
        }

        if (lastname.isEmpty()) {
            editTextLastName.setError("Last name is required to continue");
            editTextLastName.requestFocus();
            return;
        }

        if (firstname.isEmpty()) {
            editTextFirstName.setError("First name is required to continue");
            editTextFirstName.requestFocus();
            return;
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            editTextEmail.setError("Please enter a valid email address");
            editTextEmail.requestFocus();
            return;
        }

        if(password.length() < 6){
            editTextPassword.setError("Minimum length of your password should be 6 characters");
            editTextPassword.requestFocus();
            return;
        }

        //Set Progress Bar to visible so the user knows it is working
        progressBar.setVisibility(View.VISIBLE);

        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        //Turn off the progress bar
                        progressBar.setVisibility(View.GONE);

                        if (task.isSuccessful()){
                            //Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success");
                            FirebaseUser user = firebaseAuth.getCurrentUser();
                            Toast.makeText(getApplicationContext(), "User Registered Successfully", Toast.LENGTH_LONG).show();

                            //Get the userID and save the entire user (sans password) to the user portion of the database
                            SignUpActivity.this.addUserToDatabase(email, username, firstname, lastname, dateOfBirth);
                        } else {
                            Toast.makeText(getApplicationContext(), "Something went wrong",Toast.LENGTH_LONG).show();
                        }
                    }
                });


    }

    @Override
    public void onClick(View view){
        switch (view.getId()){
            case R.id.buttonSignUp:
                registerUser();
                break;

            case R.id.textViewLogin:
                startActivity(new Intent(this, LoginActivity.class));
                break;
        }
    }

    private void addUserToDatabase(String email, String username, String firstname, String lastname, String dateOfBirth){


        String userID = firebaseAuth.getCurrentUser().getUid();

        try {
            //get the current users id value


            //create the item based on what was put in the GUI
            User currentUser = new User(userID, email, firstname, lastname, username, dateOfBirth);

            //store the item into a database by the id value
            databaseReference.child("user").child(userID).child("details").setValue(currentUser);


        } catch (NullPointerException e){
            e.printStackTrace();
        }


        Toast.makeText(this,  "New User Added!", Toast.LENGTH_LONG).show();
        Toast.makeText(this, "This users id is " + userID, Toast.LENGTH_LONG).show();




    }
}
