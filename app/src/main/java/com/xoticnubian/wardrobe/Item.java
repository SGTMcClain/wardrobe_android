/*
 * 
 */
package com.xoticnubian.wardrobe;

public class Item {
	private String id; //unique id provided by database
	private String name; //provided by user
	private String description; // provided by user
	private Double purchasePrice; // provided by user
	private String brand; // dropdown that could also be provided by user 
	private String color; // dropdown that could also br provided by user
	private String type; //provide by user
	private String size;

	
	
	/**
	 * Instantiates a new empty item.
	 */
	public Item() {
		
	}
	
	/**
	 * Instantiates a new item with provided name, brand and primary color
	 * 
	 * @param name
	 * @param brand
	 * @param color
	 */
	public Item(String name, String brand, String color) {
		this.name = name;
		this.brand = brand;
		this.color = color;
		
	}

	public Item(String itemId, String name, String description, String color, String type, String size) {
		this.id = itemId;
		this.name = name;
		this.description = description;
		this.color = color;
		this.type = type;
		this.size = size;
	}

	//Item methods
	
	public void printDescription() {
		System.out.println(String.format("Item Name: %s, Brand: %s, Primary Color: %s", this.name, this.brand, this.color));
	}
	
	
	//Getters and Setters
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the purchasePrice
	 */
	public Double getPurchasePrice() {
		return purchasePrice;
	}
	/**
	 * @param purchasePrice the purchasePrice to set
	 */
	public void setPurchasePrice(Double purchasePrice) {
		this.purchasePrice = purchasePrice;
	}
	/**
	 * @return the brand
	 */
	public String getBrand() {
		return brand;
	}
	/**
	 * @param brand the brand to set
	 */
	public void setBrand(String brand) {
		this.brand = brand;
	}
	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}
	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the size
	 */
	public String getSize() {
		return size;
	}
	/**
	 * @param size the size to set
	 */
	public void setSize(String size) {
		this.size = size;
	}
}
