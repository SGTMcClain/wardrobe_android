package com.xoticnubian.wardrobe;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class ItemList extends ArrayAdapter<Item> { //Should be ArrayAdapter<Item> but I need to flesh out the item class
    private Activity context;
    private List<Item> itemList;

    public ItemList(Activity context, List<Item> itemList){
        super(context, R.layout.activity_closet_display, itemList);

        this.context = context;
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();

        View listViewItem = inflater.inflate(R.layout.list_layout, null, true);

        TextView textViewName = listViewItem.findViewById(R.id.textViewName);
        TextView textViewType = listViewItem.findViewById(R.id.textViewType);
        TextView textViewDescription = listViewItem.findViewById(R.id.textViewDescription);
        TextView textViewColor = listViewItem.findViewById(R.id.textViewColor);
        TextView textViewSize = listViewItem.findViewById(R.id.textViewSize);

        Item item = itemList.get(position);

        textViewName.setText(item.getName());
        textViewType.setText(item.getType());
        textViewDescription.setText(item.getDescription());
        textViewColor.setText(item.getColor());
        textViewSize.setText(item.getSize());

        return listViewItem;

    }
}
